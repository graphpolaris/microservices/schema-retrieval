module git.science.uu.nl/graphpolaris/schema-retrieval

go 1.22

require (
	git.science.uu.nl/graphpolaris/go-common v0.0.0-20241031214319-63909ef46ef3
	github.com/arangodb/go-driver v1.6.4
	github.com/neo4j/neo4j-go-driver/v4 v4.4.7
	github.com/rs/zerolog v1.33.0
	github.com/stretchr/testify v1.9.0
)

require (
	github.com/arangodb/go-velocypack v0.0.0-20200318135517-5af53c29c67e // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rogpeppe/go-internal v1.9.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
