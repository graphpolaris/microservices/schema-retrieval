# Schema Retrieval Package

This is the package used by the [schema service](https://git.science.uu.nl/datastrophe/microservices-backbone/schema-service) to retrieve and convert the schema.

## Retriever Interface

Any new database schema retriever needs to implement the `Retriever` interface found in `interface.go`.

## Creating a Schema Retrieval Service

```go
import "git.science.uu.nl/datastrophe/schema-retrieval/arangodb"

arangodbService := arangodb.NewService()
```

## Retrieving the Schema

```go
schema, err := arangodbService.RetrieveSchema(databaseInfo entity.DatabaseInfo, userID string, databaseName string)
```
