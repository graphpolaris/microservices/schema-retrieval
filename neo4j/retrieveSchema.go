/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package neo4j

import (
	"errors"
	"fmt"
	"log"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	"git.science.uu.nl/graphpolaris/schema-retrieval/schemaEntity"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j/db"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j/dbtype"
)

/*
RetrieveSchema makes a mock version of a neo4j schema based on the neo4j moviedatabase

	databaseInfo: entity.DatabaseInfo, the database info of the schema
	userID: string, the ID of the client
	databaseName: string, the name of the database
	Return: (*entity.JSONReturnFormat, error), the schema to be retrieved and an error if there is one
*/
func (s *Retriever) RetrieveSchema(databaseInfo *models.DBConnectionModel, userID string) (*schemaEntity.JSONReturnFormat, error) {
	// Connect to the database, use neo4j+s to use TLS
	dbURI := fmt.Sprintf("%v%v:%v", databaseInfo.Protocol, databaseInfo.URL, databaseInfo.Port)

	driver, err := neo4j.NewDriver(dbURI, neo4j.BasicAuth(databaseInfo.Username, databaseInfo.Password, ""))
	if err != nil {
		log.Println(fmt.Sprintf("SCHEMA-RETRIEVAL: %s", err.Error()))
		return nil, err
	}
	defer driver.Close()

	// Create a session
	session := driver.NewSession(neo4j.SessionConfig{
		DatabaseName: databaseInfo.InternalDatabaseName,
		AccessMode:   neo4j.AccessModeRead,
	})
	if err != nil {
		log.Println(fmt.Sprintf("SCHEMA-RETRIEVAL: %s", err.Error()))
		return nil, err
	}
	defer session.Close()

	// Get the schema
	q1, err := sendQuery(session, "call db.schema.visualization()")

	if err != nil {
		return nil, err
	}

	// Get the nodes
	q2, err := sendQuery(session, "call db.schema.nodeTypeProperties()")

	if err != nil {
		return nil, err
	}

	// Get the edges
	q3, err := sendQuery(session, "call db.schema.relTypeProperties()")

	if err != nil {
		return nil, err
	}

	// Retrieve query result
	queryOneResult, ok := q1.([]*db.Record) //[]*db.Record
	if !ok {
		return nil, errors.New("Interface casting failed")
	}

	// Take out the nodes and edges
	res1 := *queryOneResult[0] // keys: nodes relationships
	recordNodes, ok := res1.Get("nodes")
	if !ok {
		return nil, errors.New("Node collecting failed")
	}

	recordEdges, ok := res1.Get("relationships")
	if !ok {
		return nil, errors.New("Edge collecting failed")
	}

	// Convert the nodes and edges to arrays
	nodeInterfaces, ok := recordNodes.([]interface{}) // []dbtype.Node
	if !ok {
		return nil, errors.New("Node casting failed")
	}

	edgeInterfaces, ok := recordEdges.([]interface{}) // []dbtype.Relationship
	if !ok {
		return nil, errors.New("Edge casting failed")
	}

	// Make arrays of the readable formats
	nodes := make([]dbtype.Node, 0)
	edges := make([]dbtype.Relationship, 0)

	// Convert individual nodes
	for _, n := range nodeInterfaces {
		node, ok := n.(dbtype.Node)
		if !ok {
			continue
		}
		nodes = append(nodes, node)
	}

	// Convert individual edges
	for _, e := range edgeInterfaces {
		edge, ok := e.(dbtype.Relationship)
		if !ok {
			continue
		}
		edges = append(edges, edge)
	}

	nodeAttr, ok := q2.([]*db.Record)
	if !ok {
		return nil, errors.New("NodeProperties result casting failed")
	}

	edgeAttr, ok := q3.([]*db.Record)
	if !ok {
		return nil, errors.New("EdgeProperties result casting failed")
	}

	schema := formSchema(nodeAttr, edgeAttr, nodes, edges)

	return schema, nil
}
