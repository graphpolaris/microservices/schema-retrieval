package neo4j

import (
	"errors"
	"strings"

	"git.science.uu.nl/graphpolaris/schema-retrieval/schemaEntity"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j/db"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j/dbtype"
)

/*
Service is a struct used to store this use case in
*/
type Retriever struct {
}

func New() *Retriever {
	return &Retriever{}
}

// sendQuery sends a readonly query given a session
func sendQuery(session neo4j.Session, query string) (interface{}, error) {
	result, err := session.ReadTransaction(func(transaction neo4j.Transaction) (interface{}, error) {
		result, err := transaction.Run(query, nil)
		if err != nil {
			return nil, err
		}

		// Collects all results and puts them in []*db.Record format
		res, err := result.Collect()
		if err != nil {
			return nil, err
		}

		return res, nil
	})
	if err != nil {
		return nil, err
	}

	return result, err
}

// parseSchema parses the result of the query "CALL db.schema.visualization"
func parseSchema(schemaNodes []dbtype.Node, schemaEdges []dbtype.Relationship) (map[int64]string, []schemaEntity.NeoEdge) {

	// key is the identity; value is the name
	nodes := make(map[int64]string)
	relations := make([]schemaEntity.NeoEdge, 0)

	for _, n := range schemaNodes {
		// Labels[0] is where the name of the node is
		nodes[n.Id] = n.Labels[0]
	}

	for _, r := range schemaEdges {
		relations = append(relations, schemaEntity.NeoEdge{Name: r.Type, ID: r.Id, From: r.StartId, To: r.EndId})
	}

	return nodes, relations

	//The return: map[identity]nodename and a list of edges
	//This way you can fill easily get the names of the nodes, given the ID's of the from and to node
}

// getNodeAttributes parses the result of the query "CALL db.schema.nodeTypeProperties"
func getNodeAttributes(result []*db.Record) map[string][]schemaEntity.Attribute {
	nodes := make(map[string][]schemaEntity.Attribute)

	// For each node, parse the fields into attributes and append them for each node
	for _, record := range result {
		// Entity labels
		recordLabels, ok := record.Get("nodeLabels")
		if !ok {
			continue
		}

		// Labels are more or less the type of entity. E.g. Person or Movie, they can overlap such as Person and Actor
		labelSlice, ok := recordLabels.([]interface{})
		if !ok {
			continue
		}

		labels := make([]string, len(labelSlice))
		for i, n := range labelSlice {
			// Codewise a tiny bit unsafe, but it will only be strings though
			labels[i] = n.(string)
		}

		// Attribute name
		recordAttrName, ok := record.Get("propertyName")
		if !ok {
			continue
		}

		attrName, ok := recordAttrName.(string)
		if !ok {
			continue
		}

		// Attribute type (string, int etc)
		recordAttrType, ok := record.Get("propertyTypes")
		if !ok {
			continue
		}

		typeSlice, ok := recordAttrType.([]interface{})
		if !ok {
			continue
		}

		attrTypeNeo, ok := typeSlice[0].(string)
		if !ok {
			continue
		}

		attrType, err := dataTypeSwitch(attrTypeNeo)

		if err != nil {
			continue
		}

		for _, label := range labels {
			// Add the attribute to the map
			nodes[label] = append(nodes[label], schemaEntity.Attribute{Name: attrName, Type: attrType})
		}
	}

	return nodes
}

// getEdgeAttributes parses the result of the query "CALL db.schema.relTypeProperties"
func getEdgeAttributes(result []*db.Record) map[string][]schemaEntity.Attribute {
	relations := make(map[string][]schemaEntity.Attribute)

	// For each relation, parse the fields into attributes and append them for each relation
	for _, record := range result {
		// Relation names
		recordName, ok := record.Get("relType")
		if !ok {
			continue
		}

		// Always in the form of ":`REL_NAME`"
		names, ok := recordName.(string)
		if !ok {
			continue
		}
		name := strings.Split(names, "`")[1]

		recordAttrName, ok := record.Get("propertyName")
		if !ok {
			continue
		}

		//C heck to see if the edge has attributes
		if recordAttrName != nil {
			// Name of the attribute
			attrName, ok := recordAttrName.(string)
			if !ok {
				continue
			}

			// Type of the attribute
			recordAttrTypes, ok := record.Get("propertyTypes")
			if !ok {
				continue
			}

			attrTypes, ok := recordAttrTypes.([]interface{})
			if !ok {
				continue
			}

			attrTypeNeo4j, ok := attrTypes[0].(string)
			if !ok {
				continue
			}

			attrType, err := dataTypeSwitch(attrTypeNeo4j)

			if err != nil {
				continue
			}

			// Add the attribute to the map
			relations[name] = append(relations[name], schemaEntity.Attribute{Name: attrName, Type: attrType})
		} else {
			// Edge with no attributes
			relations[name] = make([]schemaEntity.Attribute, 0)
		}
	}

	return relations
}

// dataTypeSwitch is a switch to change the naming conventions of neo4j to the ones used in GraphPolaris
func dataTypeSwitch(inputType string) (string, error) {
	var dataType string

	switch inputType {
	//Neo4j types	--> GraphPolaris datatypes
	//String 		--> string
	//Date 			--> ?
	//Long 			--> int
	//StringArray 	--> ?
	//Double 		--> float
	//? (Boolean?)	--> bool

	case "String":
		dataType = "string"
		break

	case "Date":
		dataType = "string"
		break

	case "Long":
		dataType = "int"
		break

	case "StringArray":
		dataType = "string"
		break

	case "Double":
		dataType = "float"
		break

	case "Boolean": //THIS IS A GUESS
		dataType = "bool"
		break

	default:
		//This means dont handle it; prob.
		return "", errors.New("Datatype not recognised")
	}

	return dataType, nil
}

// formSchema is the function that knits the lists and maps together to create the schema
func formSchema(nodeRes []*db.Record, relationRes []*db.Record, schemeNodes []dbtype.Node, schemeEdges []dbtype.Relationship) *schemaEntity.JSONReturnFormat {
	schema := schemaEntity.JSONReturnFormat{}

	// Transform the Neo4j datatypes to more accesable formats
	nodes, relations := parseSchema(schemeNodes, schemeEdges)
	nodeAttr := getNodeAttributes(nodeRes)
	edgeAttr := getEdgeAttributes(relationRes)

	// Convert the nodes to the return format
	for _, nodeName := range nodes {
		if nodeAttr[nodeName] != nil {
			node := schemaEntity.Node{Name: nodeName, Attributes: nodeAttr[nodeName]}
			schema.Nodes = append(schema.Nodes, node)
		} else {
			node := schemaEntity.Node{Name: nodeName, Attributes: make([]schemaEntity.Attribute, 0)}
			schema.Nodes = append(schema.Nodes, node)
		}
	}

	// Convert the edges to the return format
	for _, rel := range relations {
		edge := schemaEntity.Edge{
			Name:       rel.Name,
			Collection: rel.Name,
			From:       nodes[rel.From],
			To:         nodes[rel.To],
			Attributes: edgeAttr[rel.Name],
		}
		schema.Edges = append(schema.Edges, edge)
	}

	return &schema
}
