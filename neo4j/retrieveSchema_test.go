package neo4j

import (
	"fmt"
	"testing"

	"github.com/neo4j/neo4j-go-driver/v4/neo4j/db"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j/dbtype"
	"github.com/stretchr/testify/assert"
)

func TestNodeParsing(t *testing.T) {
	// The workarounds are necessary
	vals0 := make([]interface{}, 0)
	a := make([]interface{}, 0)
	a = append(a, "Genre")
	b := make([]interface{}, 0)
	b = append(b, "User")
	c := make([]interface{}, 0)
	c = append(c, "String")
	vals0 = append(vals0, ":`Genre`", a, "name", c, true)
	vals1 := make([]interface{}, 0)
	vals1 = append(vals1, ":`User`", b, "name", c, true)
	keys := []string{"nodeType", "nodeLabels", "propertyName", "propertyTypes", "mandatory"}

	r0 := db.Record{
		Keys:   keys,
		Values: vals0,
	}
	r1 := db.Record{
		Keys:   keys,
		Values: vals1,
	}

	records := []*db.Record{&r0, &r1}

	nodes := getNodeAttributes(records)
	assert.Equal(t, fmt.Sprint(nodes), "map[Genre:[{name string}] User:[{name string}]]")
}

func TestEdgeParsing(t *testing.T) {
	// The workarounds are necessary
	vals0 := make([]interface{}, 0)
	b := make([]interface{}, 0)
	b = append(b, "Double")
	vals0 = append(vals0, ":`IN_GENRE`", nil, nil, false)
	vals1 := make([]interface{}, 0)
	vals1 = append(vals1, ":`RATED`", "rating", b, true)
	keys := []string{"relType", "propertyName", "propertyTypes", "mandatory"}

	r0 := db.Record{
		Keys:   keys,
		Values: vals0,
	}
	r1 := db.Record{
		Keys:   keys,
		Values: vals1,
	}

	records := []*db.Record{&r0, &r1}

	edges := getEdgeAttributes(records)
	assert.Equal(t, fmt.Sprint(edges), "map[IN_GENRE:[] RATED:[{rating float}]]")
}

func TestSchemaParsing(t *testing.T) {
	n0 := dbtype.Node{
		Id:     1,
		Labels: []string{"Movie"},
		Props:  nil,
	}
	n1 := dbtype.Node{
		Id:     2,
		Labels: []string{"Person"},
		Props:  nil,
	}

	e0 := dbtype.Relationship{
		Id:      7,
		StartId: 2,
		EndId:   1,
		Type:    "LIKES",
		Props:   nil,
	}

	nodes := []dbtype.Node{n0, n1}
	edges := []dbtype.Relationship{e0}

	parsedNodes, parsedEdges := parseSchema(nodes, edges)

	assert.Equal(t, fmt.Sprint(parsedNodes), "map[1:Movie 2:Person]")
	assert.Equal(t, fmt.Sprint(parsedEdges), "[{LIKES 7 2 1}]")
}

func TestDataSwitch(t *testing.T) {
	str, _ := dataTypeSwitch("String")
	num, _ := dataTypeSwitch("Long")
	num2, _ := dataTypeSwitch("Double")

	assert.Equal(t, str, "string")
	assert.Equal(t, num, "int")
	assert.Equal(t, num2, "float")
}

func TestEntireSchema(t *testing.T) {
	//Nodes
	vals0 := make([]interface{}, 0)
	a := make([]interface{}, 0)
	a = append(a, "Genre")
	b := make([]interface{}, 0)
	b = append(b, "User")
	c := make([]interface{}, 0)
	c = append(c, "String")
	vals0 = append(vals0, ":`Genre`", a, "name", c, true)
	vals1 := make([]interface{}, 0)
	vals1 = append(vals1, ":`User`", b, "name", c, true)
	keys := []string{"nodeType", "nodeLabels", "propertyName", "propertyTypes", "mandatory"}

	r0 := db.Record{
		Keys:   keys,
		Values: vals0,
	}
	r1 := db.Record{
		Keys:   keys,
		Values: vals1,
	}

	records := []*db.Record{&r0, &r1}

	//Edges
	vals2 := make([]interface{}, 0)
	d := make([]interface{}, 0)
	d = append(d, "Double")
	vals2 = append(vals2, ":`IN_GENRE`", nil, nil, false)
	keys2 := []string{"relType", "propertyName", "propertyTypes", "mandatory"}

	r3 := db.Record{
		Keys:   keys2,
		Values: vals2,
	}

	records2 := []*db.Record{&r3}

	//Schema
	n0 := dbtype.Node{
		Id:     1,
		Labels: []string{"Genre"},
		Props:  nil,
	}
	n1 := dbtype.Node{
		Id:     2,
		Labels: []string{"User"},
		Props:  nil,
	}

	e0 := dbtype.Relationship{
		Id:      7,
		StartId: 2,
		EndId:   1,
		Type:    "IN_GENRE",
		Props:   nil,
	}

	schemaNodes := []dbtype.Node{n0, n1}
	schemaEdges := []dbtype.Relationship{e0}

	// The order of keys differs, so sometimes this test will fail
	// TODO: Use something like deep equality to check if the underlying keys/values are equal
	schema := formSchema(records, records2, schemaNodes, schemaEdges)
	assert.Equal(t, fmt.Sprint(*schema), "{[{Genre [{name string}]} {User [{name string}]}] [{IN_GENRE IN_GENRE User Genre []}]}")
}

func TestRemote(t *testing.T) {

}
