/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package neo4j

import (
	"encoding/json"
	"errors"
	"fmt"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	"git.science.uu.nl/graphpolaris/schema-retrieval/schemaEntity"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j/db"
	"github.com/rs/zerolog/log"
)

/*
RetrieveSchema makes a mock version of a neo4j schema based on the neo4j moviedatabase

	databaseInfo: entity.DatabaseInfo, the database info of the schema
	userID: string, the ID of the client
	databaseName: string, the name of the database
	Return: (*entity.JSONReturnFormat, error), the schema to be retrieved and an error if there is one
*/
func (s *Retriever) RetrieveSchemaStats(databaseInfo *models.DBConnectionModel, userID string) (*schemaEntity.SchemaStats, error) {
	// Connect to the database, use neo4j+s to use TLS
	dbURI := fmt.Sprintf("%v%v:%v", databaseInfo.Protocol, databaseInfo.URL, databaseInfo.Port)

	driver, err := neo4j.NewDriver(dbURI, neo4j.BasicAuth(databaseInfo.Username, databaseInfo.Password, ""))
	if err != nil {
		return nil, err
	}
	defer driver.Close()

	// Create a session
	session := driver.NewSession(neo4j.SessionConfig{
		DatabaseName: databaseInfo.InternalDatabaseName,
		AccessMode:   neo4j.AccessModeRead,
	})
	if err != nil {
		return nil, err
	}
	defer session.Close()

	ret := &schemaEntity.SchemaStats{}

	// Get the count
	q1, err := sendQuery(session, "MATCH (n) RETURN count(n) as count")
	if err != nil {
		return nil, err
	}

	queryOneResult, ok := q1.([]*db.Record) //[]*db.Record
	if !ok {
		return nil, errors.New("failed to cast query result to []*db.Record")
	}

	count, ok := queryOneResult[0].Get("count")
	if !ok {
		return nil, errors.New("failed to get count from query result")
	}

	// Get the per nodes count
	q, err := sendQuery(session, `
		MATCH (n) 
		WITH distinct labels(n) as nodeLabel, count(*) as nodeCount
		CALL {
			WITH nodeLabel
			MATCH (n) 
			WHERE labels(n) in [nodeLabel] UNWIND keys(n) AS dkeys
			WITH distinct dkeys as dkey, nodeLabel
			CALL {
				WITH dkey, nodeLabel
				MATCH (n)
				WHERE labels(n) in [nodeLabel] AND n[dkey] IS NOT NULL 
				WITH DISTINCT valueType(n[dkey]) as attributeType
				RETURN attributeType
			}

			RETURN {key:dkey, type:attributeType} as att
		}
		RETURN nodeLabel, nodeCount, collect(att) as attributes
	`)
	if err != nil {
		return nil, err
	}

	qResult, ok := q.([]*db.Record)
	if !ok {
		return nil, errors.New("failed to cast query result to []*db.Record")
	}

	ret.Nodes.Stats = map[string]schemaEntity.NodeStats{}
	ret.Nodes.Count = 0
	for _, record := range qResult {
		labels, ok := record.Get("nodeLabel")

		if !ok {
			return nil, errors.New("failed to get label from query result")
		}
		numberOfNodes, ok := record.Get("nodeCount")
		if !ok {
			return nil, errors.New("failed to get count from query result")
		}
		attributes, ok := record.Get("attributes")
		if !ok {
			return nil, errors.New("failed to get attributes from query result")
		}

		attributesNeo := []AttributeNeo4j{}
		attributesByte, err := json.Marshal(attributes)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(attributesByte, &attributesNeo)
		if err != nil {
			return nil, err
		}

		labelStr := labels.([]interface{})[0].(string)
		attributesStats, err := getAttributesStats(session, labelStr, attributesNeo, false)
		if err != nil {
			return nil, err
		}

		ret.Nodes.Stats[labelStr] = schemaEntity.NodeStats{
			Key:            labels.([]interface{})[0].(string),
			Labels:         labels.([]interface{}),
			Count:          int(numberOfNodes.(int64)),
			AttributeStats: attributesStats,
		}

		ret.Nodes.Count += ret.Nodes.Stats[labelStr].Count
		log.Trace().Str("label", labelStr).Interface("attributeStats", attributesStats).Msg("Retrieved attribute stats")
	}

	// Get the per relation count
	q, err = sendQuery(session, `
		MATCH ()-[r]-() 
		WITH distinct type(r) as edgeLabel, count(*) as edgeCount
		CALL {
			WITH edgeLabel
			OPTIONAL MATCH ()-[r]-() 
			WHERE type(r) in [edgeLabel] UNWIND (CASE keys(r) WHEN [] THEN [null] ELSE keys(r) END) AS dkeys
			WITH distinct dkeys as dkey, edgeLabel
			CALL {
				WITH dkey, edgeLabel
				OPTIONAL MATCH ()-[r]-() 
				WHERE type(r) in [edgeLabel] AND r[dkey] IS NOT NULL 
				WITH DISTINCT valueType(r[dkey]) as attributeType
				RETURN attributeType
			}

			RETURN {key:dkey, type:attributeType} as att
		}
		RETURN edgeLabel, edgeCount, collect(att) as attributes
		`)

	if err != nil {
		return nil, err
	}

	qResult, ok = q.([]*db.Record)
	if !ok {
		return nil, errors.New("failed to cast query result to []*db.Record")
	}

	ret.Edges.Stats = map[string]schemaEntity.EdgeStats{}
	ret.Edges.Count = 0

	for _, record := range qResult {
		label, ok := record.Get("edgeLabel")

		if !ok {
			return nil, errors.New("failed to get label from query result")
		}
		numberOfEdges, ok := record.Get("edgeCount")
		if !ok {
			return nil, errors.New("failed to get count from query result")
		}
		attributes, ok := record.Get("attributes")
		if !ok {
			return nil, errors.New("failed to get attributes from query result")
		}

		attributesNeo := []AttributeNeo4j{}
		attributesByte, err := json.Marshal(attributes)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(attributesByte, &attributesNeo)
		if err != nil {
			return nil, err
		}

		labelStr := label.(string)
		attributesStats, err := getAttributesStats(session, labelStr, attributesNeo, true)
		if err != nil {
			return nil, err
		}

		ret.Edges.Stats[labelStr] = schemaEntity.EdgeStats{
			Type:           labelStr,
			Count:          int(numberOfEdges.(int64)),
			AttributeStats: attributesStats,
		}

		ret.Edges.Count += ret.Edges.Stats[labelStr].Count
		log.Trace().Str("label", labelStr).Interface("attributeStats", attributesStats).Msg("Retrieved attribute stats of edge")
	}

	// end
	log.Info().Str("userID", userID).Str("databaseName", databaseInfo.InternalDatabaseName).Any("count q", q1).Any("count", count).Msg("Retrieved node count")
	return ret, nil
}

// func getAttributesStatsByType(session neo4j.Session, label string, attributes []AttributeNeo4j, key string, entity string, isEdge bool) (map[string][]AttributeNeo4j, error) {
// 	attributesMap := make(map[string][]AttributeNeo4j)
// 	for _, attribute := range attributes {
// 		attributesMap[attribute.Type] = append(attributesMap[attribute.Type], attribute)
// 	}

// 	for key, value := range attributesMap {
// 		attributesStats, err := getAttributesStats(session, label, value, isEdge)
// 		if err != nil {
// 			return nil, err
// 		}
// 		for k, v := range attributesStats {
// 			attributesMap[k] = v
// 		}
// 	}

// 	return attributesMap, nil
// }

func getAttributesStats(session neo4j.Session, label string, attributes []AttributeNeo4j, isEdge bool) (map[string]schemaEntity.AttributeStats, error) {
	return map[string]schemaEntity.AttributeStats{}, nil

	// Ignoring attribute calculation for now due to performace issue
	// entity := "n"
	// if isEdge {
	// 	entity = "r"
	// }

	// attributesMap := make(map[string][]AttributeNeo4j)
	// for _, attribute := range attributes {
	// 	attributesMap[attribute.Type] = append(attributesMap[attribute.Type], attribute)
	// }

	// attributesStats := map[string]schemaEntity.AttributeStats{}
	// for key, values := range attributesMap {
	// 	start := time.Now()
	// 	log.Trace().Str("label", label).Str("type", key).Msg("Retrieving attribute stats for attributes of type")

	// 	dataType := mapDataType(key)
	// 	// attrStats := schemaEntity.AttributeStats{Name: record.Key, Type: mapDataType(record.Type)}

	// 	switch dataType {
	// 	case "boolean", "string":
	// 		countQuery := fmt.Sprintf("MATCH (%s:%s) RETURN", entity, label)
	// 		for value := range values {
	// 			countQuery += fmt.Sprintf(" count(DISTINCT %s.%s) AS %sUniqueCount ", entity, value, value)
	// 		}
	// 		if isEdge {
	// 			countQuery := fmt.Sprintf("MATCH (a)-[%s:%s]-(b) RETURN", entity, label)
	// 			for value := range values {
	// 				countQuery += fmt.Sprintf(" count(DISTINCT %s.%s) AS %sUniqueCount ", entity, value, value)
	// 			}
	// 		}
	// 		countResult, err := sendQuery(session, countQuery)
	// 		if err != nil {
	// 			return nil, err
	// 		}
	// 		countRecord, ok := countResult.([]*db.Record)
	// 		if !ok {
	// 			return nil, errors.New("failed to cast count result to []*db.Record")
	// 		}
	// 		uniqueCount, ok := countRecord[0].Get("uniqueCount")
	// 		if !ok {
	// 			return nil, errors.New("failed to get unique count from query result")
	// 		}
	// 		attrStats.UniqueCount = int(uniqueCount.(int64))
	// 		if uniqueCount.(int64) <= 20 {
	// 			categoryCountQuery := fmt.Sprintf("MATCH (%s:%s) WITH %s.%s AS value RETURN value, count(*) AS count", entity, label, entity, record.Key)
	// 			if isEdge {
	// 				categoryCountQuery = fmt.Sprintf("MATCH (a)-[%s:%s]-(b) WITH %s.%s AS value RETURN value, count(*) AS count", entity, label, entity, record.Key)
	// 			}
	// 			categoryCountResult, err := sendQuery(session, categoryCountQuery)
	// 			if err != nil {
	// 				return nil, err
	// 			}
	// 			categoryCountRecord, ok := categoryCountResult.([]*db.Record)
	// 			if !ok {
	// 				return nil, errors.New("failed to cast category count result to []*db.Record")
	// 			}
	// 			categoryCounts := make(map[string]int)
	// 			for _, cr := range categoryCountRecord {
	// 				value, _ := cr.Get("value")
	// 				count, _ := cr.Get("count")
	// 				categoryCounts[fmt.Sprintf("%v", value)] = int(count.(int64))
	// 			}
	// 			attrStats.CategoryCounts = categoryCounts
	// 		}

	// 	case "number":
	// 		statsQuery := fmt.Sprintf("MATCH (%s:%s) RETURN min(%s.%s) AS min, max(%s.%s) AS max, avg(%s.%s) AS avg, stDev(%s.%s) AS stdev", entity, label, entity, record.Key, entity, record.Key, entity, record.Key, entity, record.Key)
	// 		if isEdge {
	// 			statsQuery = fmt.Sprintf("MATCH (a)-[%s:%s]-(b) RETURN min(%s.%s) AS min, max(%s.%s) AS max, avg(%s.%s) AS avg, stDev(%s.%s) AS stdev", entity, label, entity, record.Key, entity, record.Key, entity, record.Key, entity, record.Key)
	// 		}
	// 		statsResult, err := sendQuery(session, statsQuery)
	// 		if err != nil {
	// 			return nil, err
	// 		}
	// 		statsRecord, ok := statsResult.([]*db.Record)
	// 		if !ok {
	// 			return nil, errors.New("failed to cast stats result to []*db.Record")
	// 		}
	// 		attrStats.MinValue, _ = statsRecord[0].Get("min")
	// 		attrStats.MaxValue, _ = statsRecord[0].Get("max")
	// 		attrStats.AvgValue, _ = statsRecord[0].Get("avg")
	// 		attrStats.StDevValue, _ = statsRecord[0].Get("stdev")

	// 	case "datetime":
	// 		dateQuery := fmt.Sprintf("MATCH (%s:%s) WHERE %s.%s IS NOT NULL RETURN min(%s.%s) AS first, max(%s.%s) AS last", entity, label, entity, record.Key, entity, record.Key, entity, record.Key)
	// 		if isEdge {
	// 			dateQuery = fmt.Sprintf("MATCH (a)-[%s:%s]-(b) WHERE %s.%s IS NOT NULL RETURN min(%s.%s) AS first, max(%s.%s) AS last", entity, label, entity, record.Key, entity, record.Key, entity, record.Key)
	// 		}
	// 		dateResult, err := sendQuery(session, dateQuery)
	// 		if err != nil {
	// 			return nil, err
	// 		}
	// 		dateRecord, ok := dateResult.([]*db.Record)
	// 		if !ok {
	// 			return nil, errors.New("failed to cast date result to []*db.Record")
	// 		}
	// 		attrStats.FirstDate, _ = dateRecord[0].Get("first")
	// 		attrStats.LastDate, _ = dateRecord[0].Get("last")

	// 	case "location":
	// 		pointQuery := fmt.Sprintf("MATCH (%s:%s) RETURN min(%s.%s.x) AS minX, max(%s.%s.x) AS maxX, min(%s.%s.y) AS minY, max(%s.%s.y) AS maxY", entity, label, entity, record.Key, entity, record.Key, entity, record.Key, entity, record.Key)
	// 		if isEdge {
	// 			pointQuery = fmt.Sprintf("MATCH (a)-[%s:%s]-(b) RETURN min(%s.%s.x) AS minX, max(%s.%s.x) AS maxX, min(%s.%s.y) AS minY, max(%s.%s.y) AS maxY", entity, label, entity, record.Key, entity, record.Key, entity, record.Key, entity, record.Key)
	// 		}
	// 		pointResult, err := sendQuery(session, pointQuery)
	// 		if err != nil {
	// 			return nil, err
	// 		}
	// 		pointRecord, ok := pointResult.([]*db.Record)
	// 		if !ok {
	// 			return nil, errors.New("failed to cast point result to []*db.Record")
	// 		}
	// 		minX, _ := pointRecord[0].Get("minX")
	// 		maxX, _ := pointRecord[0].Get("maxX")
	// 		minY, _ := pointRecord[0].Get("minY")
	// 		maxY, _ := pointRecord[0].Get("maxY")
	// 		attrStats.MinValue = fmt.Sprintf("x: %v, y: %v", minX, minY)
	// 		attrStats.MaxValue = fmt.Sprintf("x: %v, y: %v", maxX, maxY)

	// 	default:
	// 		log.Warn().Str("label", label).Str("attribute", record.Key).Str("type", attrStats.Type).Msg("Unsupported attribute type encountered")
	// 	}

	// 	attributesStats[record.Key] = attrStats

	// 	log.Trace().Dur("time", time.Since(start)).Msgf("took %v", time.Since(start))
	// 	log.Trace().Str("label", label).Dur("time", time.Since(start)).Interface("attributeStats", attrStats).Msg("Retrieved attribute stats for attribute")
	// }

	// return attributesStats, nil
}

func mapDataType(neo4jType string) string {
	switch neo4jType {
	case "BOOLEAN", "BOOLEAN NOT NULL":
		return "boolean"
	case "STRING", "STRING NOT NULL":
		return "string"
	case "INTEGER", "FLOAT", "INTEGER NOT NULL", "FLOAT NOT NULL":
		return "number"
	case "DATE", "LOCAL DATETIME", "ZONED DATETIME", "DATE NOT NULL", "LOCAL DATETIME NOT NULL", "ZONED DATETIME NOT NULL", "DURATION", "DURATION NOT NULL", "LOCAL TIME", "ZONED TIME", "LOCAL TIME NOT NULL", "ZONED TIME NOT NULL":
		return "datetime"
	case "POINT", "POINT NOT NULL":
		return "location"
	default:
		return "unknown"
	}
}

type AttributeNeo4j struct {
	Key  string `json:"key"`
	Type string `json:"type"`
}

// WHEN type in ["INTEGER", "FLOAT", "INTEGER NOT NULL", "FLOAT NOT NULL"] THEN [min(n[dkey]), max(n[dkey]), avg(n[dkey]), stDev(n[dkey])]

// WHEN type in ["STRING NOT NULL", "STRING", "BOOLEAN NOT NULL", "BOOLEAN"] THEN [count(DISTINCT n[dkey])]
