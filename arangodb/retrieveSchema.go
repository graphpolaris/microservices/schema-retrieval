/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package arangodb

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	"git.science.uu.nl/graphpolaris/schema-retrieval/schemaEntity"
	"git.science.uu.nl/graphpolaris/schema-retrieval/utils"
	"github.com/arangodb/go-driver"
	"github.com/arangodb/go-driver/http"
)

func (s *Retriever) RetrieveSchemaStats(databaseInfo *models.DBConnectionModel, userID string) (*schemaEntity.SchemaStats, error) {
	return nil, nil //TODO
}

/*
RetrieveSchema will retrieve all collections and attributes, analyse these and create a schema made up of edges and nodes

	databaseInfo: entity.DatabaseInfo, the database info containing a hostname, port, username, password and internal database name
	userID: string, the ID of the client
	databaseName: string, external database name
	Return: (entity.JSONReturnFormat, error), a json struct with the nodes and edges needed to form the schema and possible error
*/
func (s *Retriever) RetrieveSchema(databaseInfo *models.DBConnectionModel, userID string) (*schemaEntity.JSONReturnFormat, error) {

	var nodes []schemaEntity.Node
	var edges []schemaEntity.Edge

	ctx := context.Background()
	db, err := createDatabaseConnection(ctx, databaseInfo)
	if err != nil {
		log.Println("Failed to connect to database")
		return nil, err
	}

	cols, err := db.Collections(ctx)
	if err != nil {
		log.Println("Failed to retrieve the collection")
		return nil, err
	}

	for _, collection := range cols {
		collectionProperties, err := collection.Properties(ctx)
		if err != nil {
			log.Println("Failed to retrieve the properties")
			return nil, err
		}
		if !collectionProperties.IsSystem {
			// collectiontype 3 = edge collection, 2 = node collection
			if collectionProperties.Type == 2 {
				nodes = queryNodeCollection(ctx, db, collection.Name(), nodes)
			} else if collectionProperties.Type == 3 {
				edges = queryEdgeCollection(ctx, db, collection.Name(), edges)
			}
		}
	}

	jsReturn := &schemaEntity.JSONReturnFormat{
		Nodes: nodes,
		Edges: edges,
	}

	return jsReturn, nil
}

/*
Creates the connection with an arangodb, makes a client and retrieves the database of that client

	ctx: context.Context, global context
	databaseInfo: entity.DatabaseInfo, the database info containing a hostname, port, username, password and internal database name
	Return: driver.Database, database and a possible error
*/
func createDatabaseConnection(ctx context.Context, databaseInfo *models.DBConnectionModel) (driver.Database, error) {
	conn, err := http.NewConnection(http.ConnectionConfig{
		Endpoints: []string{fmt.Sprintf("%s:%d", databaseInfo.URL, databaseInfo.Port)},
		TLSConfig: &tls.Config{InsecureSkipVerify: true},
	})
	if err != nil {
		log.Println("Failed to connect to the database")
		return nil, err
	}
	client, err := driver.NewClient(driver.ClientConfig{
		Connection:     conn,
		Authentication: driver.BasicAuthentication(databaseInfo.Username, databaseInfo.Password),
	})
	if err != nil {
		log.Println("Could not log in to the ArangoDB")
		return nil, err
	}

	db, err := client.Database(ctx, databaseInfo.InternalDatabaseName)
	if err != nil {
		log.Println(err)
		log.Println("Database " + databaseInfo.InternalDatabaseName + " not found")
		return nil, err
	}

	return db, err
}

/*
queryCollection sends a query to the database to create a cursor which can access the nodes or edges
It then uses the nodeEdgeRetriever to retrieve either the nodes or edges for the schema

	ctx: context.Context, a global context
	db: driver.Database, database driver to query and read the database
	collectionName: string, the name of the collection to be queried,
	nodeEdgeRetriever: entity.NodeEdgeRetriever, a retriever function that is used to parse the cursor of either a node or edge collection
*/
func queryNodeCollection(ctx context.Context, db driver.Database, collectionName string, nodes []schemaEntity.Node) []schemaEntity.Node {
	query := "FOR d IN " + collectionName + " RETURN d"
	cursor, err := db.Query(ctx, query, nil)
	if err != nil {
		// handle error
	}
	defer cursor.Close()
	return getNodeFromCollection(ctx, cursor, collectionName, nodes)
}

/*
queryCollection sends a query to the database to create a cursor which can access the nodes or edges
It then uses the nodeEdgeRetriever to retrieve either the nodes or edges for the schema

	ctx: context.Context, a global context
	db: driver.Database, database driver to query and read the database
	collectionName: string, the name of the collection to be queried,
	nodeEdgeRetriever: entity.NodeEdgeRetriever, a retriever function that is used to parse the cursor of either a node or edge collection
*/
func queryEdgeCollection(ctx context.Context, db driver.Database, collectionName string, edges []schemaEntity.Edge) []schemaEntity.Edge {
	query := "FOR d IN " + collectionName + " RETURN d"
	cursor, err := db.Query(ctx, query, nil)
	if err != nil {
		// handle error
	}
	defer cursor.Close()
	return getEdgesFromCollection(ctx, cursor, collectionName, edges)
}

/*
getNodeFromCollection reads through all documents for a given cursor
Since a node collection can only contain one type of nodes we check for all unique attributes contained in the collection
and add them to the list for a specific node collection

	ctx: context.Context, global context
	cursor: driver.Cursor, database cursor containing all database data for a collection
	collectioName: string, name of the collection
*/
func getNodeFromCollection(ctx context.Context, cursor driver.Cursor, collectionName string, nodes []schemaEntity.Node) []schemaEntity.Node {

	var attributes []schemaEntity.Attribute
	var attributeToAdd schemaEntity.Attribute

	for {
		var doc schemaEntity.Document
		_, err := cursor.ReadDocument(ctx, &doc)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Print("Could not read document in cursor")
		}
		// Add the attributes to the set of the edge
		for attribute, value := range doc {
			if attribute[0] == '_' {
				continue
			}
			attributeToAdd, err = createAttribute(attribute, value)
			if err != nil {
				log.Print("Nested attribute marshal error")
			}

			// If the list contains the value already as int but an attribute is found as float, change the typing to float
			if !utils.Contains(attributes, attributeToAdd) {
				if attributeToAdd.Type == "float" {
					if utils.Contains(attributes, schemaEntity.Attribute{Name: attributeToAdd.Name, Type: "int"}) {
						utils.ChangeToFloat(attributes, schemaEntity.Attribute{Name: attributeToAdd.Name, Type: "int"})
					} else {
						attributes = append(attributes, attributeToAdd)
					}
				} else {
					attributes = append(attributes, attributeToAdd)
				}
			}
		}
	}
	if attributes[0].Name == "" {
		nodes = append(nodes, schemaEntity.Node{Name: collectionName, Attributes: make([]schemaEntity.Attribute, 0)})
	} else {
		nodes = append(nodes, schemaEntity.Node{Name: collectionName, Attributes: attributes})

	}
	return nodes
}

/*
getEdgesFromCollection looks at the edges and attributes in the cursor and adds them to global edges list

	ctx: context.Context, global context
	cursor: driver.Cursor, database cursor containing all database data for a collection
	collectioName: string, name of the collection
*/
func getEdgesFromCollection(ctx context.Context, cursor driver.Cursor, collectionName string, edges []schemaEntity.Edge) []schemaEntity.Edge {

	var edgeMap = make(map[string][]schemaEntity.Attribute)
	var attributeToAdd schemaEntity.Attribute
	for {
		var doc schemaEntity.Document
		_, err := cursor.ReadDocument(ctx, &doc)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			// handle other errors
		}
		// Parse the from to and the link (edge name)
		from := strings.Split(fmt.Sprintf("%s", doc["_from"]), "/")[0]
		to := strings.Split(fmt.Sprintf("%s", doc["_to"]), "/")[0]
		link := from + ":" + to

		// Add the attributes to the set of the edge
		for attribute, value := range doc {
			if attribute[0] == '_' {
				continue
			}
			attributeToAdd, err = createAttribute(attribute, value)
			if err != nil {
				log.Print("Nested attribute marshal error")
			}
		}
		if !utils.Contains(edgeMap[link], attributeToAdd) {
			edgeMap[link] = append(edgeMap[link], attributeToAdd)
		}
	}

	for edge, attributes := range edgeMap {
		split := strings.Split(edge, ":")
		if attributes[0].Name == "" {
			edges = append(edges, schemaEntity.Edge{Name: edge, Collection: collectionName, From: split[0], To: split[1], Attributes: make([]schemaEntity.Attribute, 0)})
		} else {
			edges = append(edges, schemaEntity.Edge{Name: edge, Collection: collectionName, From: split[0], To: split[1], Attributes: attributes})
		}
	}
	return edges
}

/*
createAttribute creates a single attribute that can be added to the list of attributes of either a node or edge

	attribute: string, attribute is the name of the attribute
	value: interface{}, the value is the value of the attribute
	Return: entity.Attribute, an attribute with a name and a type, possible error
*/
func createAttribute(attribute string, value interface{}) (schemaEntity.Attribute, error) {
	var attributeToAdd schemaEntity.Attribute
	switch value.(type) {
	case int:
		attributeToAdd = schemaEntity.Attribute{Name: attribute, Type: "int"}
	case float64:
		// Try to cast the float64 to an int, if this fails, it is a float64
		if !utils.TryCastToInt(value) {
			attributeToAdd = schemaEntity.Attribute{Name: attribute, Type: "float"}
		} else {
			// if the cast is succesful it is actually an int
			attributeToAdd = schemaEntity.Attribute{Name: attribute, Type: "int"}
		}
	case bool:
		attributeToAdd = schemaEntity.Attribute{Name: attribute, Type: "bool"}
	case string:
		attributeToAdd = schemaEntity.Attribute{Name: attribute, Type: "string"}
	default:
		b, err := json.Marshal(value)
		if err != nil {
			return attributeToAdd, err
		}
		attributeToAdd = schemaEntity.Attribute{Name: string(b), Type: "string"}
	}

	return attributeToAdd, nil
}
