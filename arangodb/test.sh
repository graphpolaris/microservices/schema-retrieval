#This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
#© Copyright Utrecht University (Department of Information and Computing Sciences)


echo "This is the test file"
# Download the flights dataset
# Downloading airports.json
wget -O data/airports.json http://vda-lab.github.io/assets/airports.json

# Downloading flights.json
wget -O data/flights.json http://vda-lab.github.io/assets/flights.json

# Start Arangodb docker image
DOCKER_ID=`docker run -d -p 8529:8529 -e ARANGO_ROOT_PASSWORD=root arangodb:latest`

# Copy the files into the docker container
docker cp data/airports.json $DOCKER_ID:airports.json
docker cp data/flights.json $DOCKER_ID:flights.json

# Sleep for 5 seconds to allow the container to be fully started
sleep 5s

# Import the JSON data into the arangodb instance
docker exec -i $DOCKER_ID arangoimport --file "airports.json" --type "json" --collection airports --batch-size 512000000 --create-collection
docker exec -i $DOCKER_ID arangoimport --file "flights.json" --type "json" --collection flights --batch-size 512000000 --create-collection --create-collection-type edge

# Import the airports into the 'development' database as well for testing
docker exec -i $DOCKER_ID arangoimport --file "airports.json" --type "json" --collection airports --batch-size 512000000 --create-collection --create-database --server.database development

# Run the tests and create a coverage file
go test -v -coverpkg=./... -coverprofile=cover.out ./...
go tool cover -html=cover.out -o cover.html

# Delete the cover.out file
rm cover.out

# Stop the container
docker kill $DOCKER_ID

# Remove the container
docker container rm $DOCKER_ID
