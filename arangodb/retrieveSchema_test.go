/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package arangodb

import (
	"fmt"
	"testing"

	"git.science.uu.nl/graphpolaris/schema-retrieval/schemaEntity"
	"github.com/stretchr/testify/assert"
)

/*
Tests the attribute creation

	t: *testing.T, make go recognise this as a test
*/
func TestCreateAttributesInt(t *testing.T) {

	tests := []struct {
		name      string
		attribute string
		value     interface{}
	}{
		{
			name:      "testString",
			attribute: "testStringName",
			value:     "teststring",
		},
		{
			name:      "testInt",
			attribute: "testIntName",
			value:     46,
		},
		{
			name:      "testBoolean",
			attribute: "testBooleanName",
			value:     true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testAttribute, _ := createAttribute(tt.attribute, tt.value)
			assert.Equal(t, schemaEntity.Attribute{Name: tt.attribute, Type: fmt.Sprintf("%T", tt.value)}, testAttribute)
		})
	}
}
