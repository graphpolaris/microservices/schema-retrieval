/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package main

import (
	"encoding/json"
	"io/ioutil"
	"log"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	"git.science.uu.nl/graphpolaris/schema-retrieval/neo4j"
)

/*
The main package is used to test the retrieve schema functionality, prints the parsed json result to a file for debugging purposes.
*/
func main() {
	// schemaservice := arangodb.NewService()
	// dbInfo := entity.DatabaseInfo{Hostname: "http://167.71.8.197", Port: 8529, Username: "root", Password: "DikkeDraak", InternalDatabaseName: "TweedeKamer"}
	// result, _ := schemaservice.RetrieveSchema(dbInfo, "test", "_system")
	// file, _ := json.MarshalIndent(result, "", " ")
	// ioutil.WriteFile("test.json", file, 0644)

	dbInfo := &models.DBConnectionModel{
		URL:                  "151e734c.databases.neo4j.io",
		InternalDatabaseName: "Test",
		Port:                 7687,
		Protocol:             "neo4j+s://",
		Username:             "neo4j",
		Password:             "DikkeDraak",
	}

	neo4j := neo4j.New()
	result, err := neo4j.RetrieveSchema(dbInfo, "Test")
	if err != nil {
		log.Println(err)
	}
	file, _ := json.MarshalIndent(result, "", " ")
	ioutil.WriteFile("test.json", file, 0644)
}
