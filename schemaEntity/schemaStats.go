/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package schemaEntity

/*
MessageQueueMessage describes a message coming in through the message queue
*/
type SchemaStats struct {
	Nodes NodeMetadata `json:"nodes"`
	Edges EdgeMetadata `json:"edges"`
}

type NodeMetadata struct {
	Count int                  `json:"count"`
	Stats map[string]NodeStats `json:"stats"`
}

type EdgeMetadata struct {
	Count int                  `json:"count"`
	Stats map[string]EdgeStats `json:"stats"`
}

type NumericStats struct {
	MinValue interface{} `json:"minValue,omitempty"`
}

type NodeStats struct {
	Key            string                    `json:"key"`
	Labels         []interface{}             `json:"labels"`
	Count          int                       `json:"count"`
	AttributeStats map[string]AttributeStats `json:"attributeStats"`
}

type EdgeStats struct {
	Type           string                    `json:"type"`
	Count          int                       `json:"count"`
	AttributeStats map[string]AttributeStats `json:"attributeStats"`
}

type AttributeStats struct {
	Name           string         `json:"name"`
	Type           string         `json:"type"`
	MinValue       interface{}    `json:"minValue,omitempty"`
	MaxValue       interface{}    `json:"maxValue,omitempty"`
	AvgValue       interface{}    `json:"avgValue,omitempty"`
	StDevValue     interface{}    `json:"StDevValue,omitempty"`
	UniqueCount    int            `json:"uniqueCount,omitempty"`
	CategoryCounts map[string]int `json:"categoryCounts,omitempty"`
	FirstDate      interface{}    `json:"firstDate,omitempty"`
	LastDate       interface{}    `json:"lastDate,omitempty"`
}
