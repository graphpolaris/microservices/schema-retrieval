/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package schemaEntity

import (
	"context"

	"github.com/arangodb/go-driver"
)

/*
Document with Empty struct to retrieve all data from the DB Document
*/
type Document map[string]interface{}

/*
NodeEdgeRetriever is a function that creates a node or edge set
*/
type NodeEdgeRetriever func(context.Context, driver.Cursor, string)

/*
JSONReturnFormat is the format in which the schema will be returned, so it can be easily converted to JSON
*/
type JSONReturnFormat struct {
	Nodes []Node `json:"nodes"`
	Edges []Edge `json:"edges"`
}

/*
Node is a JSON format for nodes
*/
type Node struct {
	Name       string      `json:"name"`
	Attributes []Attribute `json:"attributes"`
}

/*
Edge is a JSON format for edges
*/
type Edge struct {
	Name       string      `json:"name"`
	Collection string      `json:"collection"`
	From       string      `json:"from"`
	To         string      `json:"to"`
	Attributes []Attribute `json:"attributes"`
}

/*
Attribute is a JSON format for attributes, Type should onle be one of "string", "boolean", "int", "float"
*/
type Attribute struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

/*
NeoEdge is an edge that holds the information from a relation returned by the schema query. It is yet to be parsed to a format that GraphPolaris can use
*/
type NeoEdge struct {
	Name string
	ID   int64
	From int64
	To   int64
}
