/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package schema

import (
	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	"git.science.uu.nl/graphpolaris/schema-retrieval/arangodb"
	"git.science.uu.nl/graphpolaris/schema-retrieval/neo4j"
	"git.science.uu.nl/graphpolaris/schema-retrieval/schemaEntity"
	"github.com/rs/zerolog/log"
)

/*
Interface for a database schema retriever
*/
type Retriever interface {
	RetrieveSchema(databaseInfo *models.DBConnectionModel, userID string) (*schemaEntity.JSONReturnFormat, error)
	RetrieveSchemaStats(databaseInfo *models.DBConnectionModel, userID string) (*schemaEntity.SchemaStats, error)
}

func GetRetriever(engine string) Retriever {
	switch engine {
	case "arangodb":
		return arangodb.New()
	case "neo4j":
		return neo4j.New()
	}

	log.Error().Str("engine", engine).Msg("unknown schema retrieval engine")
	return nil
}
