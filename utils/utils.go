/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package utils

import "git.science.uu.nl/graphpolaris/schema-retrieval/schemaEntity"

/*
Contains is a function to check if an attribute is already contained in a list of attributes

	attributes: []entity.Attribute, List of attributes
	searchTerm: entity.Attribute, attribute to check for
	Return: bool, True if the search term is in the list, False if the list does not contain the search term
*/
func Contains(attributes []schemaEntity.Attribute, searchTerm schemaEntity.Attribute) bool {
	for _, attribute := range attributes {
		if attribute.Name == searchTerm.Name && attribute.Type == searchTerm.Type {
			return true
		}
	}
	return false
}

/*
ChangeToFloat changes the type for int to float for a specified searchTerm

	attributes: []entity.Attribute, List of attributes
	searchTerm: entity.Attribute, attribute to check for
*/
func ChangeToFloat(attributes []schemaEntity.Attribute, searchTerm schemaEntity.Attribute) {
	for i := 0; i < len(attributes); i++ {
		if attributes[i].Name == searchTerm.Name && attributes[i].Type == searchTerm.Type {
			attributes[i].Type = "float"
		}
	}
}

/*
TryCastToInt takes in an interface of unknowns type and tries to cast it to an Int
If this fails the input is a Float

	f: interface{}, interface of unknown type
	Return: bool, True if int, False if float
*/
func TryCastToInt(f interface{}) bool {
	fl, _ := f.(float64)
	return fl == float64(int(fl))
}
