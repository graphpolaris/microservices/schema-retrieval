/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package utils

import (
	"testing"

	"git.science.uu.nl/graphpolaris/schema-retrieval/schemaEntity"
	"github.com/stretchr/testify/assert"
)

/*
TestTryCastToInt test the float and int casting
*/
func TestTryCastToInt(t *testing.T) {
	isInt := TryCastToInt(1.0)
	isFloat := TryCastToInt(1.5)

	assert.Equal(t, true, isInt)
	assert.Equal(t, false, isFloat)
}

/*
TestIntToFloat tests if the type of the attribute is correctly changed to a float
This is done by first adding an attribute with the type int to a list and then trying the add
the same attribute with the value float afterwards.
This is done to prevent wrongfull int casts as the value of 1.0 will be cast to an int but if later on the
value of 1.5 is encounered it will otherwise still stay as an int type.

	t: *testing.T, makes go recognise it as a test
*/
func TestIntToFloat(t *testing.T) {
	attributes := make([]schemaEntity.Attribute, 0)
	attributeToAdd := schemaEntity.Attribute{Name: "intToFloatTest", Type: "float"}
	attributes = append(attributes, schemaEntity.Attribute{Name: "intToFloatTest", Type: "int"})

	if !Contains(attributes, attributeToAdd) {
		if attributeToAdd.Type == "float" {
			if Contains(attributes, schemaEntity.Attribute{Name: attributeToAdd.Name, Type: "int"}) {
				ChangeToFloat(attributes, schemaEntity.Attribute{Name: attributeToAdd.Name, Type: "int"})
			} else {
				attributes = append(attributes, attributeToAdd)
			}
		} else {
			attributes = append(attributes, attributeToAdd)
		}
	}

	var correctAttributes []schemaEntity.Attribute
	correctAttributes = append(correctAttributes, attributeToAdd)
	assert.Equal(t, correctAttributes, attributes)
}

/*
TestContains tests if contains function is working correctly
It first tests if the attributes in the list can be found for every possible type, then it checks an attribute that is not in the list
Finally it tests an attribute with a matching name, but unmatched type

	t: *testing.T, makes go recognise it as a test
*/
func TestContains(t *testing.T) {
	var attributeList []schemaEntity.Attribute
	attributeString := schemaEntity.Attribute{Name: "stringTest", Type: "string"}
	attributeBoolean := schemaEntity.Attribute{Name: "boolTest", Type: "boolean"}
	attributeInteger := schemaEntity.Attribute{Name: "intTest", Type: "int"}
	attributeFloat := schemaEntity.Attribute{Name: "floatTest", Type: "float"}
	attributeNotInList := schemaEntity.Attribute{Name: "not in list", Type: "string"}
	attrSameNameDiffType := schemaEntity.Attribute{Name: "stringTest", Type: "boolean"}

	attributeList = append(attributeList, attributeString, attributeBoolean, attributeInteger, attributeFloat)

	stringInList := Contains(attributeList, attributeString)
	boolInList := Contains(attributeList, attributeBoolean)
	intInList := Contains(attributeList, attributeInteger)
	floatInList := Contains(attributeList, attributeFloat)
	notInList := Contains(attributeList, attributeNotInList)
	sameNameNotInList := Contains(attributeList, attrSameNameDiffType)

	assert.Equal(t, true, stringInList)
	assert.Equal(t, true, boolInList)
	assert.Equal(t, true, intInList)
	assert.Equal(t, true, floatInList)
	assert.Equal(t, false, notInList)
	assert.Equal(t, false, sameNameNotInList)
}
